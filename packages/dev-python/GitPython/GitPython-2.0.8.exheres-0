# Copyright 2015 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=setuptools blacklist='3.2' test=nose ]
# The tests use the git repo as reference data, so we use scm-git even for releases
# NOTE(moben): maybe extend github.exlib to allow using git for releases as well, but so far only
# gitdb and GitPython need it
SCM_TAG=${PV}
SCM_REPOSITORY="https://github.com/gitpython-developers/${PN}"
SCM_gitdb_REPOSITORY="https://github.com/gitpython-developers/gitdb"
SCM_smmap_REPOSITORY="https://github.com/gitpython-developers/smmap"

SCM_SECONDARY_REPOSITORIES="gitdb smmap"
SCM_EXTERNAL_REFS="git/ext/gitdb:gitdb"
SCM_gitdb_EXTERNAL_REFS="gitdb/ext/smmap:smmap"

require scm-git

SUMMARY="A python library used to interact with Git repositories"
DESCRIPTION="
It provides abstractions of git objects for easy access of repository data, and additionally allows
you to access the git repository more directly using either a pure python implementation, or the
faster, but more resource intensive git command implementation."
HOMEPAGE="https://github.com/gitpython-developers/${PN}"
REMOTE_IDS="github:gitpython-developers/${PN}"
UPSTREAM_DOCUMENTATION="http://gitpython.readthedocs.org/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/gitdb[>=0.6.4][python_abis:*(-)?]
        dev-scm/git
    test:
        dev-python/mock[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-tests.patch
)

prepare_one_multibuild() {
    # tests expect HEAD == master, set master to the commit we want, so everyone is happy
    edo git checkout -B master ${PV}

    # tests need a working git setup
    edo git config --global user.name "paludis"
    edo git config --global user.email "paludis@example.com"

    # tests need a non-empty reflog https://github.com/gitpython-developers/GitPython/issues/286
    edo git commit --allow-empty --message "Empty commit to please tests"

    setup-py_prepare_one_multibuild
}

test_one_multibuild() {
    port=$((RANDOM+1024))
    esandbox allow_net "inet:127.0.0.1@${port}"
    esandbox allow_net --connect "inet:127.0.0.1@${port}"

    export GIT_PYTHON_TEST_GIT_DAEMON_PORT=${port}
    export TMPDIR=${TEMP}/GitPython-testdir$(python_get_abi)
    edo mkdir "${TMPDIR}"

    export LC_ALL=en_GB.utf8
    setup-py_test_one_multibuild
    unset LC_ALL

    unset GIT_PYTHON_TEST_GIT_DAEMON_PORT
    TMPDIR=${TEMP}

    esandbox disallow_net --connect "inet:127.0.0.1@${port}"
    esandbox disallow_net "inet:127.0.0.1@${port}"
}

