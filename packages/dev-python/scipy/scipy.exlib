# Copyright 2012 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi flag-o-matic
# use github releases, pypi only provides the sources as zip
require github [ suffix=tar.gz release=v${PV} ]

SETUP_CFG_COMMANDS="ALL"

SETUP_CFG_ALL_PARAMS=(
    "include_dirs = /usr/$(exhost --target)/include"
    "library_dirs = /usr/$(exhost --target)/lib"
)

require setup-py [ import=distutils cfg_file=site ]

SUMMARY="SciPy is python framework for mathematics, science, and engineering."
DESCRIPTION="
SciPy (pronounced 'Sigh Pie') is open-source software for mathematics,
science, and engineering.  It includes modules for statistics, optimization,
integration, linear algebra, Fourier transforms, signal and image processing,
ODE solvers, and more.

The SciPy library is built to work with NumPy arrays, and provides many
user-friendly and efficient numerical routines such as routines for numerical
integration and optimization.

It adds significant power to the interactive Python session by exposing the
user to high-level commands and classes for the manipulation and visualization
of data. With SciPy, an interactive Python session becomes a data-processing
and system-prototyping environment rivaling sytems such as MATLAB, IDL,
Octave, R-Lab, and SciLab
"
HOMEPAGE="http://scipy.org/SciPy"

UPSTREAM_DOCUMENTATION="http://docs.scipy.org/doc/ [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

# This numpy dep should really be >=1.8.2 (see setup.py), but =1.9.2-r1
# was subtly broken in its search for fortran compilers, so require the
# -r2 version.
DEPENDENCIES="
    build+run:
        dev-python/numpy[>=1.9.2-r2][python_abis:*(-)?]
        sys-libs/libgfortran:=
        virtual/blas
        virtual/lapack
    test:
        dev-python/nose[>=1.0][python_abis:*(-)?]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( "COMPATIBILITY" "DEV_README.txt" )

prepare_one_multibuild() {
    # make sure we don't autodetect these libraries
    export {ATLAS,FFTW,MKL,OPENBLAS,PTATLAS,UMFPACK}=None

    # set correct fortran compiler
    export F90="${FORTRAN}"

    append-ldflags -shared

    setup-py_prepare_one_multibuild
}

test_one_multibuild() {
    local abi_temp="${TEMP}${MULTIBUILD_TARGET}"
    local test_dir="${abi_temp}"/test
    local test_libdir="${test_dir}"/lib
    local test_log="${abi_temp}"/test.log

    # Install scipy to a temporary directory for testing.
    edo mkdir -p "${test_libdir}"
    export PYTHONPATH="${test_libdir}"
    edo "${PYTHON}" setup.py install        \
        --home="${test_dir}"                \
        --install-lib="${test_libdir}"      \
        --single-version-externally-managed \
        --record="${abi_temp}"/record.log   \
        --no-compile

    edo pushd "${test_dir}"
    edo ${PYTHON} -c "
import scipy, sys
result = scipy.test('fast', verbose=2)
sys.exit(0 if result.wasSuccessful() else 1)"
    edo popd
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # Remove distutils-installed documentation
    edo rm -f "${IMAGE}"$(python_get_sitedir)/${PN}/*.txt
}

