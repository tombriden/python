# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Copyright 2010 Cecil Curry <leycec@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'numpy-1.3.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require pypi [ suffix=zip ] flag-o-matic

SETUP_CFG_COMMANDS="ALL"

SETUP_CFG_ALL_PARAMS=(
    "include_dirs = /usr/$(exhost --target)/include"
    "library_dirs = /usr/$(exhost --target)/lib"
)

require setup-py [ import=setuptools cfg_file=site has_bin=true ]

SUMMARY="Fast and sophisticated array facility for the Python language"
DESCRIPTION="
NumPy is the fundamental package needed for scientific computing with Python.
It contains:

  * a powerful N-dimensional array object
  * sophisticated broadcasting functions
  * basic linear algebra functions
  * basic Fourier transforms
  * sophisticated random number capabilities
  * tools for integrating Fortran code
  * tools for integrating C/C++ code

Besides its obvious scientific uses, NumPy can also be used as an efficient
multi-dimensional container of generic data. Arbitrary data-types can be
defined. This allows NumPy to seamlessly and speedily integrate with a wide
variety of databases.
"
HOMEPAGE+=" http://${PN}.scipy.org"

UPSTREAM_DOCUMENTATION="http://docs.scipy.org/doc/${PN} [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/Cython[>=0.19][python_abis:*(-)?]
        sys-libs/libgfortran:=
        virtual/blas
        virtual/lapack
    test:
        dev-python/nose[>=1.0][python_abis:*(-)?]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( "COMPATIBILITY" "DEV_README.txt" )

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # make sure we don't autodetect these libraries
    export {ATLAS,MKL,OPENBLAS,PTATLAS}=None

    # set correct fortran compiler
    export F90="${FORTRAN}"

    append-ldflags -shared
}

test_one_multibuild() {
    local abi_temp="${TEMP}${MULTIBUILD_TARGET}"
    local test_dir="${abi_temp}"/test
    local test_libdir="${test_dir}"/lib
    local test_log="${abi_temp}"/test.log

    # Install numpy to a temporary directory for testing.
    edo mkdir -p "${test_libdir}"
    export PYTHONPATH="${test_libdir}"
    export PATH="${test_dir}/bin:${PATH}"
    edo "${PYTHON}" setup.py install        \
        --home="${test_dir}"                \
        --install-lib="${test_libdir}"      \
        --single-version-externally-managed \
        --record="${abi_temp}"/record.log   \
        --no-compile

    edo pushd "${test_dir}"
    edo ${PYTHON} -c "
import numpy, sys
result = numpy.test('full', verbose=3)
sys.exit(0 if result.wasSuccessful() else 1)"
    edo popd
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # Remove installed tests
    edo rm -rf "${IMAGE}$(python_get_sitedir)"/${PN}/f2py/tests
}

